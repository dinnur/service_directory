import os
import csv
from datetime import datetime

import pandas as pd
from typing import List

from additions.db import database, Launch, Provider, FailedItem, FailedItemLaunch


def write_to_xlsx(model, items, fields_names: List[str] = None):
    if fields_names is not None:
        model.check_fields(fields_names)
        fields_filtered = {f: model._meta.fields[f].verbose_name for f in fields_names}
    else:
        fields_filtered = {f: f_ins.verbose_name for f, f_ins in model._meta.fields.items()}

    today = datetime.now()
    file_path = os.path.join(os.getcwd(), f'output_files/{today.strftime("%Y-%m-%d %H:%M:%S")}.xlsx')
    with database.atomic():
        data = {v_n if v_n else f: [getattr(i, f) for i in items] for f, v_n in fields_filtered.items()}

    with pd.ExcelWriter(file_path, engine="xlsxwriter") as writer:
        pd.DataFrame(data).to_excel(writer, index=False)
        writer.save()


def generate_city_from_csv(continue_from_last_launch: bool = False):
    with open(os.path.join(os.getcwd(), 'cities.csv')) as csv_file:
        rows = list(
            {i[0].replace('"', '') for i in csv.reader(csv_file, delimiter='\n', quotechar='|') if i}
        )
        if continue_from_last_launch and Launch.select().exists():
            last_launch = Launch.select().order_by(Launch.id.desc()).get()
            providers_q = last_launch.providers.order_by(Provider.id.desc())
            check_last_provider = (providers_q.exists() and providers_q.get().city_state in rows)
            rows = rows[rows.index(providers_q.get().city_state):] if check_last_provider else rows
        city_states = {i.city_state for i in Provider.select()}
        for row in rows:
            if row not in city_states:
                yield row
        city_states = [i for i in city_states
                       if Provider.select().where(Provider.city_state == i).count() <= 200]
        for i in city_states:
            yield i


def write_to_xlsx_last_slaunch(fields: List[str]):
    """Write to xlsx file providers of Last success overed launch"""
    launch = Launch.select().where(Launch.status == "successfully").order_by(Launch.id.desc()).get()
    if not launch.providers:
        return False
    write_to_xlsx(Provider, launch.providers, fields)
    return True
