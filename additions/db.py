import os
from datetime import datetime
from typing import List
from urllib.parse import urlparse

from playhouse.postgres_ext import JSONField, PostgresqlExtDatabase
from peewee import *


# database = SqliteDatabase(os.path.join(os.getcwd(), "services.db"))
database = PostgresqlExtDatabase(
    "postgres",
    user="test_user",
    password="password",
    host="0.0.0.0",
    port=5432
)


class URLField(CharField):
    field_type = 'CHAR'

    def __init__(self, max_length=255, *args, **kwargs):
        super().__init__(max_length, *args, **kwargs)
        self.url_ = None

    def python_value(self, value):
        if value:
            self.url_ = urlparse(value)
            if not self.url_.netloc:
                raise ValueError(f'Invalid Url input: {value}')
        return super().python_value(value)


class BaseModel(Model):

    @classmethod
    def check_fields(cls, fields: List[str]):
        error_fields = []
        for field in fields:
            if field not in cls._meta.fields.keys():
                error_fields.append(field)

        if error_fields:
            fields_error = ' '.join(error_fields)
            raise ValueError(f'Not found in model {cls.__name__}\n\tFields: {fields_error}')

    class Meta:
        database = database


class Site(BaseModel):
    url = URLField(primary_key=True)
    created_at = DateTimeField(default=datetime.now)


class Service(BaseModel):
    title = CharField()
    site = ForeignKeyField(Site, backref='services', on_delete="CASCADE")
    created_at = DateTimeField(default=datetime.now)


class Provider(BaseModel):
    url_on_site = URLField(unique=True, verbose_name='URL on site')
    name = CharField(verbose_name='Name')
    brand = CharField(verbose_name='Brand', null=True)
    city_state = CharField(verbose_name='City and State')
    address = CharField(verbose_name='Address', null=True)
    gmap_link = URLField(verbose_name='Google map', null=True)
    link = URLField(verbose_name='Site Link', null=True)
    phone = CharField(max_length=15, verbose_name='Phone', null=True)
    image = CharField(verbose_name='Image', null=True)
    services = ManyToManyField(Service, backref='providers', on_delete='CASCADE')
    descriptions = TextField(verbose_name='Descriptions', null=True)
    created_at = DateTimeField(default=datetime.now)
    updated_at = DateTimeField(default=datetime.now)


ProviderService = Provider.services.get_through_model()


class FailedItem(BaseModel):
    STATUS_CHOICES = (
        ('http_err', 'HttpError'),
        ('DNS_lookup_err', 'DNSLookupError'),
        ('timeout_err', 'TimeoutError'),
        ('item_save_err', 'Item save error')
    )
    data = JSONField(null=True)
    url_on_site = URLField()
    status = CharField(choices=STATUS_CHOICES, default='http_err')
    city_state = CharField(null=True)
    failed_at = DateTimeField(default=datetime.now)
    resolved = BooleanField(default=False)


class Launch(BaseModel):
    STATUS_CHOICES = (
        ('in_process', 'In Process'),
        ('successfully', 'Successfully'),
        ('failed', 'Failed')
    )
    providers = ManyToManyField(Provider, backref='launches', on_delete='CASCADE')
    failed_items = ManyToManyField(FailedItem, backref='launches', on_delete='CASCADE')

    status = CharField(choices=STATUS_CHOICES, default='in_process')
    started_at = DateTimeField(default=datetime.now)
    continued_at = DateTimeField(default=datetime.now)
    finished_at = DateTimeField(default=datetime.now)


FailedItemLaunch = Launch.failed_items.get_through_model()
LaunchProvider = Launch.providers.get_through_model()
