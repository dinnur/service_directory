from additions.db import (
    database, Site, Service, Provider, ProviderService, FailedItem,
    Launch, LaunchProvider, FailedItemLaunch
)


def create_tables():
    with database:
        database.create_tables([
            Site, Service, Provider, ProviderService, FailedItem,
            Launch, LaunchProvider, FailedItemLaunch
        ])


def remove_if_in_failed(launch: Launch, url: str):
    query = (
        FailedItem.select().join(FailedItemLaunch)
        .join(Launch)
        .where((Launch.id == launch.id) & (FailedItem.url_on_site == url))
    )
    if query.exists():
        query.get().resolved = True
        launch.launch.failed_items.remove(query.get())
