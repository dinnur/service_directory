# 1. Installing all dependencies
```commandline
$ pip install -r requiremets.txt
```
# 2. Create all tables in database
```commandline
# for up postgres on port 5432
$ docker-compose up -d --build  
```
```shell
>> from additions.q import create_tables
>> create_tables()
```
# 3. Run spider tbsdirectory
```commandline
$ scrapy crawl tbsdirectory
```
# 4. Create directory in base directory with name output_files
> :warning: **Required!** xlsx files will be stored in this directory:
```bash
    .
    ├── ...
    ├── service_directory       # Base dir
    │   ├── additions              
    │   ├── output_files           
    │   ├── scraper           
    │   └── ...                
    └── ...
```

# 5. After the end of spider job
```shell
>> from additions.q import *
>> from additions.utils import write_to_xlsx_last_slaunch
>> fields = ["name", "brand", "address", "gmap_link", "link", "phone", "image", "descriptions"]
>> write_to_xlsx_last_slaunch(fields)
```
