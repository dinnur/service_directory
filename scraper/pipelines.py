import json
from datetime import datetime

import urllib3
from additions.q import *


urllib3.disable_warnings()


class DatabasePipeline:
    def __init__(self, *args, **kwargs):
        self.db = database
        self.launch = None
        self.site = None
        self.services = dict()

    def open_spider(self, spider):
        assert hasattr(spider.services, '__iter__')
        assert all(isinstance(i, str) for i in spider.services)
        assert isinstance(spider.site, str)
        self.db.connect()
        self.site, _ = Site.get_or_create(url=spider.site)
        for service_title in spider.services:
            self.services[service_title], _ = Service.get_or_create(site=self.site, title=service_title)
        if spider.continue_from_last_launch and Launch.select().exists():
            spider.logger.info('Start with last launch')
            self.launch = Launch.select().order_by(Launch.id.desc()).get()
            self.launch.continued_at = datetime.now()
            self.launch.save()
        else:
            spider.logger.info('Start with new Launch')
            self.launch = Launch.create()

    def close_spider(self, spider):
        self.launch.status, self.launch.finished_at = 'successfully', datetime.now()
        self.launch.save()
        self.db.close()

    def process_item(self, item, spider):
        url = item['url_on_site']
        service = self.services[item.pop('service')]
        city_state = item['city_state']
        try:
            if item.get('item_err') and item.get('status_err'):
                data, status = item['item_err'], item['status_err']
                fail_item = FailedItem.create(data=data, url_on_site=url,
                                              status=status, city_state=city_state)
                self.launch.failed_items.add(fail_item)
                spider.failed_urls.append((url, service, city_state))
                return item
            query = Provider.select().where(Provider.url_on_site == url)
            save = False
            if not query.exists():
                provider = Provider.create(**item)
                provider.services.add(service)
                save = True
                spider.logger.info('Create new provider %s', url)
            else:
                provider = query.get()
                if service not in provider.services:
                    provider.services.add(service)
                    save = True
                for field, value in item.items():
                    if getattr(provider, field) != value:
                        setattr(provider, field, value)
                        save = True
            if save:
                provider.updated_at = datetime.now()
                provider.save()
            if provider not in self.launch.providers:
                self.launch.providers.add(provider)
        except Exception as e:
            spider.logger.info('Item Error')
            data = json.dumps({**item, "err": e.__class__.__name__, "err_text": e})
            fail_item = FailedItem.create(data=data, url_on_site=url,
                                          status='item_save_err', city_state=city_state)
            self.launch.failed_items.add(fail_item)
            spider.failed_urls.append(url)
        finally:
            return item
