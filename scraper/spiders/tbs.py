from urllib.parse import urljoin

import scrapy

from additions.utils import generate_city_from_csv
from scraper.items import ScraperItemLoader, ScraperItem


class TbsSpider(scrapy.Spider):
    name = 'tbs'
    services = ['truck-repair']
    site = 'https://tbsdirectory.com/'
    base_url = 'https://tbsdirectory.com/search?q={service}&l={address}'
    # item page xpaths
    xpaths_for_extract = {
        'brand': '//div[@class="header__body-tagline"]/text()',
        'name': '//h1[@class="header__body-text"]/text()',
        'address': '//div[@class="listing__address"]/text()',
        'gmap_link': '//div[@class="listing__address"]/a/@href',
        'phone': '//div[@class="listing__numbers"]//a/text()',
        'link': '//div[@class="listing__link"]/a/@href',
        'image': '//div[@class="listing__content-narrow"]/img/@src',
        'descriptions': '//div[@class="listing__content-wide"]//text()'
    }
    # fields for xpath.getall() else just get()
    get_all_fields = ['descriptions']
    continue_from_last_launch = False

    failed_urls = []

    def urls_from_csv(self) -> tuple:
        yield from ((self.base_url.format(service=s, address=c), c, s)
                    for c in generate_city_from_csv(self.continue_from_last_launch) if c
                    for s in self.services)

    def start_requests(self):
        for url, city_state, service in self.urls_from_csv():
            yield scrapy.Request(
                url, self.parse,
                meta={'service': self.services[0], 'city_state': city_state, 'url': url}
            )

    def parse(self, response, **kwargs):
        city_state = response.meta['city_state']
        items_pages = response.xpath('//div[@class="search__main-listings"]//a/@href').getall()
        if items_pages:
            items_pages = [urljoin(response.url, i) for i in items_pages if i.startswith('/usa')]
            for page_url in items_pages or []:
                yield scrapy.Request(
                    page_url, self.parse_page,
                    meta={'service': self.services[0], 'city_state': city_state, 'url': page_url}
                )

        next_page = response.xpath('//a[@class="search__main-pagination-jump"]/@href').getall()
        if next_page:
            next_page_url = urljoin(response.url, next_page[-1])
            yield scrapy.Request(
                next_page_url, callback=self.parse,
                meta={'service': self.services[0], 'city_state': city_state, 'url': next_page_url}
            )

    def parse_page(self, response):
        loader = ScraperItemLoader(item=ScraperItem(), response=response)
        for field, xpath in self.xpaths_for_extract.items():
            if field in self.get_all_fields:
                values = response.xpath(xpath).getall()
                loader.add_value(field, " ".join(values))
            else:
                loader.add_value(field, response.xpath(xpath).get())
        loader.add_value('url_on_site', response.url)
        loader.add_value('city_state', response.meta['city_state'])
        loader.add_value('service', response.meta['service'])
        return loader.load_item()



