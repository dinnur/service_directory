# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html
import json

import scrapy
from scrapy.loader import ItemLoader
from itemloaders.processors import TakeFirst, MapCompose, Compose


def json_dumps(obj=None):
    return json.dumps(obj) if obj is not None else None


def clear_str(value: str = None):
    return value.strip().replace('\n', '').replace('\r', '') if value else None


def remove_extra_spaces(value: str = None):
    value = remove_extra_str(value)
    return ' '.join([i for i in clear_str(value).split(' ') if i != '']) if value else None


def remove_extra_str(value: str = None):
    return value.replace('(adsbygoogle = window.adsbygoogle || []).push({});', '') if value else None


class ScraperItem(scrapy.Item):
    url_on_site = scrapy.Field()
    name = scrapy.Field()
    brand = scrapy.Field()
    address = scrapy.Field()
    gmap_link = scrapy.Field()
    link = scrapy.Field()
    image = scrapy.Field()
    phone = scrapy.Field()
    descriptions = scrapy.Field()
    service = scrapy.Field()
    city_state = scrapy.Field()
    item_err = scrapy.Field()
    status_err = scrapy.Field()


class ScraperItemLoader(ItemLoader):
    default_output_processor = TakeFirst()

    name_in = MapCompose(clear_str)
    brand_in = MapCompose(clear_str)
    descriptions_in = MapCompose(remove_extra_spaces)
    address_in = MapCompose(clear_str)
    item_err_in = Compose(json_dumps)
