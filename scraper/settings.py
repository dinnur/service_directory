# Scrapy settings for service_directory2 project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     https://docs.scrapy.org/en/latest/topics/settings.html
#     https://docs.scrapy.org/en/latest/topics/downloader-middleware.html
#     https://docs.scrapy.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'scraper'

SPIDER_MODULES = ['scraper.spiders']
NEWSPIDER_MODULE = 'scraper.spiders'

ITEM_PIPELINES = {
    'scraper.pipelines.DatabasePipeline': 40
}

DOWNLOADER_MIDDLEWARES = {
    'scraper.middlewares.ServiceDirectoryProxy': 100,
    'scrapy.downloadermiddlewares.httpproxy.HttpProxyMiddleware': 110,
}

DOWNLOADER_CLIENTCONTEXTFACTORY = 'scrapy.core.downloader.contextfactory.ScrapyClientContextFactory'

HTTPCACHE_IGNORE_HTTP_CODES = (
    400, 401, 403, 404, 408, 429, 500, 502, 503, 504, 522, 524,
)

DOWNLOAD_DELAY = .7
DOWNLOAD_TIMEOUT = 180
CONCURRENT_REQUESTS = 1
CONCURRENT_ITEMS = 1
# CONCURRENT_REQUESTS_PER_DOMAIN = 10
CONCURRENT_REQUESTS_PER_IP = 1
RANDOMIZE_DOWNLOAD_DELAY = False

USER_AGENT = 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:15.0) Gecko/20100101 Firefox/15.0.1'

COOKIES_ENABLED = False
COOKIES_DEBUG = False

LOG_LEVEL = 'INFO'

RETRY_TIMES = 3

ROBOTSTXT_OBEY = False

# TELNETCONSOLE_ENABLED = False
